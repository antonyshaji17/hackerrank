
https://www.hackerrank.com/challenges/repeated-string/problem?isFullScreen=true&h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=warmup

function repeatedString($s, $n) {
  $stringArray = str_split($s);
  $count = count(array_filter($stringArray,function($a) {return $a=='a';}));
  $repeats = floor($n/strlen($s));
  $extras = $n%strlen($s);
  array_splice($stringArray,$extras);
  $extrasCount = count(array_filter($stringArray,function($a) {return $a=='a';}));
  return ($count*$repeats)+$extrasCount;
}

